# VCD Check

a domain specific language to change the value of VCD, supporting the development of our RISC-V core.

**File extension**

.vc , stands for vcd check

**In same clock**

[module].[wire] [operator] [value] , [color]

[module].[wire] [operator] [module].[wire] , [color]

e.g.

```
ex.pin1 = 0x1234 , red
```

**Check pin value in previous clock**

clk=[value] [module].[wire] [operator] [value] , [color]

clk=[value] [module].[wire] [operator] [module].[wire] , [color]

e.g. : 

```
clk=-2 ex.pin1 = if.pin1 , red
```
