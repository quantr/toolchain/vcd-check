package hk.quantr.vcdcheck;

import hk.quantr.javalib.CommonLib;
import hk.quantr.vcdcheck.structure.Statement;
import hk.quantr.vcdcheck.antlr.VCDCheckBaseListener;
import hk.quantr.vcdcheck.antlr.VCDCheckParser;
import hk.quantr.vcdcheck.antlr.VCDCheckParser.ParameterContext;
import hk.quantr.vcdcheck.structure.BuiltInParameter;
import hk.quantr.vcdcheck.structure.Compare;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class MyVCDCheckListener extends VCDCheckBaseListener {

	public HashMap<String, Object> initStatements = new HashMap<>();
	public HashMap<String, Object> wireNames = new HashMap<>();
	public ArrayList<Statement> statements = new ArrayList<>();

	@Override
	public void exitInitStatements(VCDCheckParser.InitStatementsContext ctx) {
		String key = ctx.major.getText();
		if (ctx.minor != null) {
			key += "." + ctx.minor.getText();
		}
		if (ctx.value().NUMBER() != null) {
			if (ctx.type == 0) {
				initStatements.put(key, Integer.parseInt(ctx.value().NUMBER().getText()));
			}
		} else if (ctx.value().STRING() != null) {
			if (ctx.type == 0) {
				String s = ctx.value().STRING().getText();
				s = s.substring(1);
				s = s.substring(0, s.length() - 1);
				initStatements.put(key, s);
			}
		} else if (ctx.value() != null) {
			if (ctx.type == 0) {
				initStatements.put(key, ctx.value().getText());
			} else if (ctx.type == 1) {
				wireNames.put(key, ctx.value().getText());
			}
		}
	}

	@Override
	public void exitHighlight(VCDCheckParser.HighlightContext ctx) {
		Statement statement = new Statement();
		for (VCDCheckParser.CheckingStatementContext checkingStatement : ctx.checking().checkingStatement()) {
			try {
				int type = checkingStatement.type;
				Compare compare = new Compare();
				compare.type = type;
				if (type == 0) {
					compare.module1 = checkingStatement.moduleWire(0).module().getText();
					compare.wire1 = checkingStatement.moduleWire(0).wire().getText();
					if (checkingStatement.moduleWire(0).offset() != null) {
						compare.offset1 = CommonLib.string2int(checkingStatement.moduleWire(0).offset().NUMBER().getText());
					}
					compare.value = CommonLib.string2long(checkingStatement.value().NUMBER().getText());
					if (checkingStatement.value().offset() != null) {
						compare.valueOffset = CommonLib.string2int(checkingStatement.value().offset().NUMBER().getText());
					}
					statement.compares.add(compare);
				} else if (type == 1) {
					compare.module1 = checkingStatement.moduleWire(0).module().getText();
					compare.wire1 = checkingStatement.moduleWire(0).wire().getText();
					if (checkingStatement.moduleWire(0).offset() != null) {
						compare.offset1 = CommonLib.string2int(checkingStatement.moduleWire(0).offset().NUMBER().getText());
					}
					compare.module2 = checkingStatement.moduleWire(1).module().getText();
					compare.wire2 = checkingStatement.moduleWire(1).wire().getText();
					if (checkingStatement.moduleWire(1).offset() != null) {
						compare.offset2 = CommonLib.string2int(checkingStatement.moduleWire(1).offset().NUMBER().getText());
					}
					statement.compares.add(compare);
				} else if (type == 2) {
					compare.module1 = checkingStatement.moduleWire(0).module().getText();
					compare.wire1 = checkingStatement.moduleWire(0).wire().getText();
					if (checkingStatement.moduleWire(0).offset() != null) {
						compare.offset1 = CommonLib.string2int(checkingStatement.moduleWire(0).offset().NUMBER().getText());
					}
					compare.builtInName = checkingStatement.builtin().name.getText();
					if (checkingStatement.builtin().parameters() != null) {

						for (ParameterContext parameter : checkingStatement.builtin().parameters().parameter()) {
							compare.builtinParameters.add(new BuiltInParameter(
									parameter.moduleWire().module().getText(),
									parameter.moduleWire().wire().getText(),
									parameter.moduleWire().offset() == null ? 0 : CommonLib.string2int(parameter.moduleWire().offset().NUMBER().getText())));
						}
					}
					if (checkingStatement.builtin().returnValue != null) {
						compare.returnValue = checkingStatement.builtin().returnValue;
					}
					statement.compares.add(compare);
				} else {
					System.out.println("unsupported type=" + type + ", " + ctx.getText());
				}
				statement.color = ctx.color().getText();
				statements.add(statement);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

}
