package hk.quantr.vcdcheck;

import hk.quantr.vcdcheck.antlr.VCDCheckLexer;
import hk.quantr.vcdcheck.antlr.VCDCheckParser;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class VCDCheck {

	public static MyVCDCheckListener check(String vcdContent) {
		VCDCheckLexer lexer = new VCDCheckLexer(CharStreams.fromString(vcdContent));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		VCDCheckParser parser = new VCDCheckParser(tokenStream);
		MyVCDCheckListener l = new MyVCDCheckListener();
		parser.addParseListener(l);
		VCDCheckParser.CompileContext context = parser.compile();
		return l;
	}
}
