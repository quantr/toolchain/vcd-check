grammar VCDCheck;

compile	: init NL+ wirename? NL+ lines
		;

init	:	INIT '{' NL* initStatements[0]* '}'
		;

wirename	:	'wirename' '{' NL* initStatements[1]* '}'
			;

initStatements	[int type]	:	major=identifier (DOT minor=identifier)? '=' value NL+
							;

lines	:	line*
		;

line	:	comment	NL+
		|	highlight NL+
		;

comment		:	'#' ~NL*
			;

highlight	:	checking COMMA color
			;

checking	:	checkingStatement ((AND|OR) checkingStatement)*
			;

checkingStatement	returns[int type]
					:	moduleWire operator builtin		{$type=2;}
					|	builtin operator moduleWire		{$type=2;}
					|	value operator builtin			{$type=3;}
					|	builtin operator value			{$type=3;}
					|	moduleWire operator value		{$type=0;}
					|	value operator moduleWire		{$type=0;}
					|	moduleWire operator moduleWire	{$type=1;}
					;

builtin		:	'builtin' DOT name=identifier parameters? offset? (DOT returnValue=identifier)?
			;

parameters	:	'(' parameter (COMMA parameter)* ')'
			;

parameter	:	identifier
			|	moduleWire
			|	NUMBER
			;

moduleWire	:	module DOT wire offset?
			;

offset		:	'(' NUMBER ')'
			;

module		:	identifier
			;

wire		:	identifier
			;

operator	:	OPERATOR
			|	EQUAL
			;

value		:	NUMBER offset?
			|	STRING offset?
			|	identifier (COMMA identifier)*
			;

color		:	COLOR
			|	HTML_COLOR
			;

identifier	returns[int type]
			:	IDENTIFIER										{$type=1;}
			|	IDENTIFIER '[' IDENTIFIER ']'					{$type=2;}
			|	IDENTIFIER '[' IDENTIFIER DOT IDENTIFIER ']'	{$type=3;}
			|	IDENTIFIER '[' NUMBER ']'						{$type=4;}
			;

INIT		:	'init'
			;

EQUAL		:	'=='
			;

OPERATOR	:	'>'
			|	'>='
			|	'<'
			|	'<='
			;

AND			:	'&&'
			;

OR			:	'||'
			;

COLOR		:	'red'
			|	'yellow'
			|	'green'
			|	'blue'
			;

NUMBER		:	'-'? '0x' [0-9abcdefABCDEF]+
			|	'-'? [0-9]+
			;

STRING		:	'"' (~'"')* '"'
			;

HTML_COLOR	:	'#' [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F] [0-9a-fA-F]
			;

DOT			:	'.'
			;

COMMA		:	','
			;

IDENTIFIER	:	[a-zA-Z] [a-zA-Z0-9_]*
			;

WS			:	(' '|'\t')+	-> channel (HIDDEN);

NL			:	'\r'? '\n';
