package hk.quantr.vcdcheck.structure;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class BuiltInParameter {

	public String module;
	public String wire;
	public int offset;

	public BuiltInParameter(String module, String wire, int offset) {
		this.module = module;
		this.wire = wire;
		this.offset = offset;
	}

	@Override
	public String toString() {
		return "" + module + "." + wire + "(" + offset + ')';
	}

}
