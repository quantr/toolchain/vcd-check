package hk.quantr.vcdcheck.structure;

import hk.quantr.vcdcheck.antlr.VCDCheckParser.IdentifierContext;
import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Compare {

	public String module1;
	public String wire1;
	public int offset1;

	public String module2;
	public String wire2;
	public int offset2;

	public long value;
	public int valueOffset;
	public int type;

	public String builtInName;
	public IdentifierContext returnValue;
	public ArrayList<BuiltInParameter> builtinParameters = new ArrayList<>();

	@Override
	public String toString() {
		if (type == 0) {
			return module1 + "." + wire1 + "(" + offset1 + ")" + " = " + value + "(" + valueOffset + ")";
		} else if (type == 1) {
			return module1 + "." + wire1 + "(" + offset1 + ")" + " = " + module2 + "." + wire2 + "(" + offset2 + ")";
		} else if (type == 2) {
			if (builtinParameters.size() == 0) {
				return module1 + "." + wire1 + "(" + offset1 + ")" + " = " + builtInName + ")." + returnValue.getText();
			} else {
				String temp = "";
				for (BuiltInParameter p : builtinParameters) {
					temp += p + ", ";
				}
				return module1 + "." + wire1 + "(" + offset1 + ")" + " = " + builtInName + "(" + temp + ")" + ")." + returnValue.getText();
			}
		}
		return "Compare{" + "module1=" + module1 + ", wire1=" + wire1 + ", offset1=" + offset1 + ", module2=" + module2 + ", wire2=" + wire2 + ", offset2=" + offset2 + ", value=" + value + ", type=" + type + '}';
	}

}
