package hk.quantr.vcdcheck.structure;

import java.util.ArrayList;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class Statement {

	public String color;
	public ArrayList<Compare> compares = new ArrayList<>();

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Statement{color=").append(color).append("\n");
		for (Compare compare : compares) {
			sb.append("\t").append(compare).append("\n");
		}
		sb.append('}');
		return sb.toString();
	}

}
