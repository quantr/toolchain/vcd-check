
import hk.quantr.vcdcheck.MyVCDCheckListener;
import hk.quantr.vcdcheck.antlr.VCDCheckLexer;
import hk.quantr.vcdcheck.antlr.VCDCheckParser;
import hk.quantr.vcdcheck.structure.Compare;
import hk.quantr.vcdcheck.structure.Statement;
import java.io.FileInputStream;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

/**
 *
 * @author Peter <peter@quantr.hk>
 */
public class TestVCDCheck {

	@Test
	public void test() throws IOException {
		VCDCheckLexer lexer = new VCDCheckLexer(CharStreams.fromStream(new FileInputStream(TestVCDCheck.class.getResource("1.vc").getPath())));
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);

		VCDCheckParser parser = new VCDCheckParser(tokenStream);
		MyVCDCheckListener l = new MyVCDCheckListener();
		parser.addParseListener(l);
		VCDCheckParser.CompileContext context = parser.compile();
		System.out.println(l.initStatements.size());
		for (Statement statement : l.statements) {
			System.out.println(">" + statement);
			Compare compare = statement.compares.get(0);
			if (compare.returnValue != null) {
				if (compare.returnValue.type == 4) {
					System.out.println("compare.returnValue=" + compare.returnValue.type + ", " + compare.returnValue.NUMBER().getText());
				} else if (compare.returnValue.type == 3) {
					System.out.println("compare.returnValue=" + compare.returnValue.type + ", " + compare.returnValue.IDENTIFIER(1));
				} else {
					System.out.println("compare.returnValue=" + compare.returnValue.type + ", " + compare.returnValue.getText());
				}
			}
			System.out.println("------------------------------------------------------------");
		}
		System.out.println("-".repeat(100));
		System.out.println(l.wireNames.size());
		for (String key : l.wireNames.keySet()) {
			System.out.println(">" + key + "=" + l.wireNames.get(key));
		}
	}
}
